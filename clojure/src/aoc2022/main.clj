(ns aoc2022.main
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(def calories-resource (io/resource
                        "aoc2022/calories-list.txt"))

(def calories-list (str/split-lines (slurp calories-resource)))

(def elves (remove #(=  '("") %) (partition-by #(= "" %) calories-list)))

(defn to-integer [x]
  (Integer/parseInt x))

(def elf-calories (map #(reduce + (map to-integer %)) elves))

(def most-elf-calories (last (sort elf-calories)))

(def greatest-3-elf-calories (take-last 3 (sort elf-calories)))

(def sum-greatest-3-elf-calories (reduce + greatest-3-elf-calories))

; (do elves)
; (do calories-list)
; (do elf-calories)
; (sort elf-calories)
; (do greatest-3-elf-calories)

(defn day1 []
    (assert (= 72718 most-elf-calories))
    (assert (= 213089 sum-greatest-3-elf-calories)))

(defn -main
  [& argv]
  (day1)
  (prn ["Most calories: " most-elf-calories])
  (prn ["Great three calories: " greatest-3-elf-calories " sum = " sum-greatest-3-elf-calories]))