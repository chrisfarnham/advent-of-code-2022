# Advent of Code 2022

Chris Farnham's repo for [Advent of Code 2022](https://adventofcode.com/2022)


## Install and run
1. Make sure Clojure is installed.
1. Run `clj -M -m aoc2022.main` in the root directory of the project

## Run tests
1. `clj -Xtest`


