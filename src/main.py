import os.path as path
from functools import reduce


def get_data():
    module_dir = path.dirname(path.abspath(__file__))
    with open(f"{module_dir}/calories-list.txt", "r") as cl:
        yield from cl.readlines()


def data_to_calories():
    values = [x.strip() for x in get_data()]
    size = len(values)
    idx_list = [idx + 1 for idx, val in enumerate(values) if val == ""]
    res = [
        values[i:j]
        for i, j in zip(
            [0] + idx_list, idx_list + ([size] if idx_list[-1] != size else [])
        )
    ]
    res = [list(filter(lambda x: x != "", l)) for l in res]
    return res


def sum_list(l):
    return reduce(lambda x, y: x + y, l)


def sum_calories():
    int_slices = [[int(x) for x in l] for l in data_to_calories()]
    return [sum_list(l) for l in int_slices]


def highest_calories(n=1):
    l = sum_calories()
    l.sort(reverse=True)
    return l[0:n]


def main():
    print(f"Highest calories: {sum_list(highest_calories())}")
    print(f"Highest three calories: {sum_list(highest_calories(3))}")


if __name__ == "__main__":
    main()
